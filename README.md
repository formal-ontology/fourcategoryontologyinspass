# Introduction

A collection of theories have been written in
[SPASS](https://www.mpi-inf.mpg.de/departments/automation-of-logic/software/spass-workbench)
to formalise a four-category ontology and Chen's original model from an
intensional perspective.

# A four-category ontology

File `fourCategoryOntology.dfg` contains an axiomatisation of a four-category
ontology, which assumes the existence of universals and particulars. A
particular is not instantiated by any entity. The inherence relationship is
established between particulars. Thus, a particular is the substrate of
particular qualities. There exist universal qualities and universal substrates
as well. A substrate that is not quality of another entity is a substance.
There are both particular and universal substances.

Two theorems (`theorem1.dfg` and `theorem2.dfg`) have been provided. To check
that they are provable, the instructions are 

```console
SPASS theorem1.dfg -DocProof
``` 

and

```console
SPASS theorem2.dfg -DocProof
``` 

respectively.

A scenario has been created to check the theory (`testsForTheFourCategoryOntology.dfg`).

To run it, the instruction is

```console
SPASS testsForTheFourCategoryOntology.dfg -DocProof
``` 

# Chen's original model from a conceptual realist perspective

The different _things_ identified by [Chen in his 1976
paper](https://web.archive.org/web/20120915192439/http://csc.lsu.edu/news/erd.pdf)
are analysed according to the following assumptions:

1. The four-category ontology axiomatised in `fourCategoryOntology.dfg`.
2.  Concepts as ways of thinking of things.

The result is formalised in file `chenModel.dfg`, which includes `fourCategoryOntology.dfg`.  
A scenario has been created to check the theory (`testsForChenModel.dfg`).

To run it, the instruction is

```console
SPASS testsForChenModel.dfg -DocProof
``` 
